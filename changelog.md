# Change Log 

## [3.2] - 2021-11-24

### Added

- Bot para raqueta1
- DatosMemCompartida                               

### Changed




## [3.1] - 2021-11-17

### Added

- Logger

### Changed

- Se ha comentado todo lo relacionado con la aparición de multiples esferas


## [2.1] - 2021-10-16
  
### Added

- Vector de esferas y sus interacciones
- Aparecen nuevas esferas cada x tiempo

### Changed
 

 
### Fixed


## [1.2] - 2021-10-07
  
### Added


### Changed

- cambio Changelog.txt a changelog.md
 
### Fixed

 
## [1.1] - 2021-10-07
  
### Added

- Changelog.txt

### Changed

- comentario en tenis.cpp
 
### Fixed
 

