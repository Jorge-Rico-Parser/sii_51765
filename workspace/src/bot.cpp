#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

int main()
{
	DatosMemCompartida *pdatosBot;
	int fd;

	fd = open("/tmp/datosBot", O_RDWR);
	pdatosBot = (DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	while(1)
	{
		if(pdatosBot->esfera.centro.y > (pdatosBot->raqueta1.y1 - 1))
			pdatosBot->accion = 1;
		if(pdatosBot->esfera.centro.y < (pdatosBot->raqueta1.y1 - 1))
                        pdatosBot->accion = -1;
		usleep(25000);
	}
}
