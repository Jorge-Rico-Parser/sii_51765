// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	munmap(pdatosBot, sizeof(DatosMemCompartida));
	unlink("/tmp/datosBot");
	close(fd_sc);
	unlink("/tmp/Serv-Cliente");
	close(fd_cs);
	unlink("/tmp/Cliente-Serv");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

/*	for(i=0;i<esferas.size();i++)
                esferas[i].Dibuja();
*/
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	//RECIBO DATOS DEL SERVIDOR
	char cad[200];
	read(fd_sc, cad, sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);

	//BOT
	pdatosBot->raqueta1 = jugador1;
        pdatosBot->esfera = esfera;
	//Movimiento del bot jugador1
	if(pdatosBot->accion == 1) OnKeyboardDown('w',0,0);
	if(pdatosBot->accion == -1) OnKeyboardDown('s',0,0);

	//Comentado la implementación de las multiples esferas
/*	int i,j;
	for(i=0;i<esferas.size();i++)
		esferas[i].Mueve(0.025f);
	for(i=0;i<paredes.size();i++)
	{
		for(j=0;j<esferas.size();j++)
		{
			paredes[i].Rebota(esferas[j]);
		}
			paredes[i].Rebota(jugador1);
			paredes[i].Rebota(jugador2);
	}

	for(j=0;j<esferas.size();j++)
	{
		jugador1.Rebota(esferas[j]);
		jugador2.Rebota(esferas[j]);

		if(fondo_izq.Rebota(esferas[j]))
		{
			esferas[j].centro.x=0;
			esferas[j].centro.y=rand()/(float)RAND_MAX;
			esferas[j].velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[j].velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;

			//mensaje pipe
			int mensaje[2];
			mensaje[0]=puntos1;
			mensaje[1]=puntos2;
			write(fd, &mensaje, sizeof(mensaje));
		}

		if(fondo_dcho.Rebota(esferas[j]))
		{
			esferas[j].centro.x=0;
			esferas[j].centro.y=rand()/(float)RAND_MAX;
			esferas[j].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esferas[j].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;

			//mensaje pipe
                        int mensaje[2];
                        mensaje[0]=puntos1;
                        mensaje[1]=puntos2;
                        write(fd, &mensaje, sizeof(mensaje));

		}
	}

	static int var=0;
	Esfera es;
	if(var == 300)
	{
		if(esferas.size() < 5)
		{
			esferas.push_back(es);
		}
		var = 0;
	}var++;
*/
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[100];
	sprintf(cad, "%c", key);
	write(fd_cs, cad, sizeof(cad));
}

void CMundo::Init()
{
	//SERVIDOR-CLIENTE
	unlink("/tmp/Serv-Cliente");
	if(mkfifo("/tmp/Serv-Cliente",0666) < 0)
	{
		perror("No puede CREARSE fichero Serv-Cliente");
	}
	if((fd_sc = open("/tmp/Serv-Cliente",O_RDONLY, 0666)) < 0)
        {
                perror("No puede ABRIRSE fichero Serv-Cliente");
        }

	//CLIENTE-SERVIDOR
	unlink("/tmp/Cliente-Servidor");
	if(mkfifo("/tmp/Cliente-Serv",0666) < 0)
        {
                perror("No puede CREARSE fichero Cliente-Serv");
        }
        if((fd_cs = open("/tmp/Cliente-Serv",O_WRONLY, 0666)) < 0)
        {
                perror("No puede ABRIRSE fichero Cliente-Serv");
        }

	//Memoria compartida para el bot
	int fd2;
	datosBot.raqueta1 = jugador1;
	datosBot.esfera = esfera;
	datosBot.accion = 0;

	if((fd2 = open("/tmp/datosBot",O_RDWR|O_CREAT|O_TRUNC, 0666)) < 0)
	{
		perror("No puede crearse fichero para bot");
	}
	write(fd2, &datosBot, sizeof(DatosMemCompartida));
	pdatosBot = (DatosMemCompartida*)mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0);
	close(fd2);

//	Esfera e;
//	esferas.push_back(e);

	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
