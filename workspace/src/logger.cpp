
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
	int fd;
	unlink("/tmp/fifo_logger");
	if((mkfifo("/tmp/fifo_logger",0666)) < 0)
	{
		perror("No pudo crearse el FIFO");
		return(1);
	}

	int puntos1 = 0, puntos2 = 0, end = 0;

	while(1)
	{
		if((fd=open("/tmp/fifo_logger",O_RDONLY)) < 0)
		{
                	perror("No pudo crearse el FIFO");
                	return(1);
		}

		read(fd, &puntos1, sizeof(int));
		read(fd, &puntos2, sizeof(int));

		printf("Jugador_1: %d puntos --- Jugador_2: %d puntos \n", puntos1, puntos2);

		read(fd, &end, sizeof(int));
		if(end == 1)
		{
			close(fd);
        		unlink("/tmp/fifo_logger");
		        exit(0);
		}
	}
}
