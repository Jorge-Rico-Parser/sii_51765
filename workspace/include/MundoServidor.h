// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "DatosMemCompartida.h"
//#include "Esfera.h"
//#include "Raqueta.h"


class CMundo
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();

	DatosMemCompartida datosBot;
	DatosMemCompartida *pdatosBot;

	Esfera esfera;
	std::vector<Plano> paredes;
//	std::vector<Esfera> esferas;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	int fd,fd_sc,fd_cs;
	pthread_t thid1;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
